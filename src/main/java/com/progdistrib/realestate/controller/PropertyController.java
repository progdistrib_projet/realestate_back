package com.progdistrib.realestate.controller;

import com.progdistrib.realestate.utils.Utils;

import java.util.List;

import com.progdistrib.realestate.dao.PropertyRepository;
import com.progdistrib.realestate.dao.UserRepository;
import com.progdistrib.realestate.entity.Appartment;
import com.progdistrib.realestate.entity.House;
import com.progdistrib.realestate.entity.Property;
import com.progdistrib.realestate.entity.User;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

@RestController
public class PropertyController {

    private Utils utils;

    private PropertyRepository propertyRepository;

    private UserRepository userRepository;

    public PropertyController(PropertyRepository propertyRepository, UserRepository userRepository) {
        this.propertyRepository = propertyRepository;
        this.userRepository = userRepository;
        this.utils = new Utils();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/properties")
    @ResponseStatus(HttpStatus.OK)
    public List<Property> getProperties() {
        return propertyRepository.findAll();


    }

    @CrossOrigin(origins = "*")
    @GetMapping("/properties/available/{idUser}")
    @ResponseStatus(HttpStatus.OK)
    public List<Property> getPropertiesRentableForUser(@PathVariable(value = "idUser") long idUser) {
        User owner = userRepository.findByIdUser(idUser);
        if (owner == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist");
        }
        List<Property> usersProperties = propertyRepository.findByOwner(owner);
        List<Property> all = propertyRepository.findAll();

        for(int i = 0; i<all.size();i++) {
            Property prop = all.get(i);
            if(usersProperties.contains(prop)) {
                all.remove(prop);
            }
        }


        if(all.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No property to rent");
        return all;
    }


    @CrossOrigin(origins = "*")
    @GetMapping("/properties/user/{idUser}")
    @ResponseStatus(HttpStatus.OK)
    public List<Property> getOwnerProperties(@PathVariable(value = "idUser") long idUser) {
        User owner = userRepository.findByIdUser(idUser);
        if (owner == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist");
        }
        return propertyRepository.findByOwner(owner);

    }

    @CrossOrigin(origins = "*")
    @GetMapping("/property/rent/{idUser}")
    @ResponseStatus(HttpStatus.OK)
    public Property getRentedProperty(@PathVariable(value = "idUser") long idUser) {
        User owner = userRepository.findByIdUser(idUser);
        if (owner == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist");
        }
        return propertyRepository.findByTenant(owner);

    }


/*
    @CrossOrigin(origins = "*")
    @GetMapping("/property/{id}")
    public Property getPropertyById(@PathVariable(value = "id") long id) {
        return propertyRepository.findById(id);
    }*/

    @CrossOrigin(origins = "*")
    @PostMapping("/house")
    public Property addHouse(@RequestBody House house) {

        /**
         * verification que l'utilisateur dans le body de la requete existe bien dans la
         * bdd. Si username n'existe pas alors retourner une exception
         */
        User userSaved = userRepository.findByUsername(house.getOwner().getUsername());
        if (userSaved == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist");

        } else {
            house.setOwner(userSaved);
            return propertyRepository.save(house);
        }

    }

    @CrossOrigin(origins = "*")
    @PostMapping("/appartment")
    public Property addAppartment(@RequestBody Appartment appartment) {

        /**
         * verification que l'utilisateur dans le body de la requete existe bien dans la
         * bdd. Si username n'existe pas alors retourner une exception
         */
        User userSaved = userRepository.findByUsername(appartment.getOwner().getUsername());
        if (userSaved == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist");

        } else {
            appartment.setOwner(userSaved);
            return propertyRepository.save(appartment);
        }

    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/property/{id}")
    public Property deleteProperty(@PathVariable long id) {
        return propertyRepository.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/property/{id}")
    public Property updateProperty(@RequestBody Property newProperty, @PathVariable(value = "id") Long id) {

        /**
         * verification que l'utilisateur dans le body de la requete existe bien dans la
         * bdd. Si username n'existe pas alors retourner une exception
         */
        Property propertySaved = propertyRepository.getOne(id);

        if (propertySaved == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Property not found");

        if (newProperty.getTenant() != null) {
            User userSaved = userRepository.findByUsername(newProperty.getTenant().getUsername());
            if (userSaved == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exist");
            }
            propertySaved.setTenant(userSaved);

        } else {
            newProperty.setOwner(propertySaved.getOwner());

            //rendre une propriete : tenant = null
            if (newProperty.isAvailable()) {
                propertySaved.setTenant(null);
            }
        }

        propertySaved.setName(utils.verifyString(newProperty.getName(), propertySaved.getName()));
        propertySaved.setAddress(utils.verifyString(newProperty.getAddress(), propertySaved.getAddress()));
        propertySaved.setRentPrice(utils.verifyLong(newProperty.getRentPrice(), propertySaved.getRentPrice()));
        propertySaved.setAvailable(newProperty.isAvailable());
        return propertyRepository.save(propertySaved);

    }

}
