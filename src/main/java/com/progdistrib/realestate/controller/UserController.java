package com.progdistrib.realestate.controller;

import java.util.List;

import com.progdistrib.realestate.dao.UserRepository;
import com.progdistrib.realestate.entity.User;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

@RestController
public class UserController {

    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Recuperer tous les users
     *
     * @return liste de users
     */
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<User> getUsers() {
        return userRepository.findAll();

    }

      /**
     * Recuperer tous les users
     *
     * @return liste de users
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin(origins = "*")
    public User getUserForLogin(@RequestBody User user) {

        User inDatabase =  userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());

        if (inDatabase != null)
            return inDatabase;
        else 
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "username and password not found");

    }


    /**
     * 
     * @param username
     * @return utilisateur ayant le username
     */
    @GetMapping("/users/{username}")
    public User getUserByUsername(@PathVariable(value = "username") String username) {

        return userRepository.findByUsername(username);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/users")
    public User createUser(@RequestBody User user){
         return userRepository.save(user);

    }
}
