package com.progdistrib.realestate.dao;

import java.util.List;

import com.progdistrib.realestate.entity.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

    List<User> findAll();

    User getOne(long idUser);

    User findByIdUser(long idUser);


}
