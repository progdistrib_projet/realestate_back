package com.progdistrib.realestate.dao;

import java.util.List;

import com.progdistrib.realestate.entity.Property;
import com.progdistrib.realestate.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;


public interface PropertyRepository extends JpaRepository<Property, Long> {


    List<Property> findAll();

    Property findById(long id);

    Property deleteById(long id);

    Property getOne(long id);

    List<Property> findByOwner(User user);

    Property findByTenant(User user);

}
