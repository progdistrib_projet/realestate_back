package com.progdistrib.realestate.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Property {

    private long idProperty;
    private String name;
    private String address;
    private int rentPrice;
    private boolean available;
    private User owner;
    private User tenant;

    // Constructeur vide
    public Property() {
    }

    // Constructeur
    public Property(String name, String address, int rentPrice, boolean available) {
        this.name = name;
        this.address = address;
        this.rentPrice = rentPrice;
        this.available = available;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdProperty() {
        return idProperty;
    }

    public void setIdProperty(long idProperty) {
        this.idProperty = idProperty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(int rentPrice) {
        this.rentPrice = rentPrice;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    //@ManyToOne
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @OneToOne(cascade = CascadeType.MERGE, optional = true)
    public User getTenant() {
        return tenant;
    }

    public void setTenant(User tenant) {
        this.tenant = tenant;
    }

    @Override
    public String toString() {
        return "Property{" + ", name=" + name + '\'' + ", address='" + address + '\'' + ", rentPrice=" + rentPrice
                + '\'' + ", available=" + available + '\'' + ", owner=" + owner.getUsername() + '\'' + ", tenant="
                + tenant.getUsername() + +'}';
    }

}
