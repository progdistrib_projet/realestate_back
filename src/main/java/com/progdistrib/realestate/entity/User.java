package com.progdistrib.realestate.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {

    private long idUser;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private Property propertyRented;
    private List<Property> propertiesOwned;

    public User() {
    }

    public User(String username, String password, String firstname, String lastname) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.propertiesOwned = new ArrayList<Property>();

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Column(unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @OneToOne(mappedBy="tenant", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JsonIgnore
    public Property getPropertyRented() {
        return propertyRented;
    }

    public void setPropertyRented(Property propertyRented) {
        this.propertyRented = propertyRented;
    }

    @OneToMany(mappedBy = "owner", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JsonIgnore
    public List<Property> getPropertiesOwned() {
        return propertiesOwned;
    }

    public void setPropertiesOwned(List<Property> propertiesO) {
        this.propertiesOwned = propertiesO;
    }

    @Override
    public String toString() {
        return "User{" + ", username=" + username + '\'' + ", firstname='" + firstname + '\'' + ", lastname=" + lastname
                + '}';
    }

}
