package com.progdistrib.realestate.entity;

import javax.persistence.Entity;

@Entity
public class House extends Property {
    
    private int floor;
    private boolean garden;
    private boolean garage;

    public House(){
        super();
    }

    public House(String name, String address, int rentPrice, boolean available, int floor, boolean garden, boolean garage) {
        super(name, address, rentPrice, available);
        this.floor = floor;
        this.garden = garden;
        this.garage = garage;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public boolean isGarden() {
        return garden;
    }

    public void setGarden(boolean garden) {
        this.garden = garden;
    }

    public boolean isGarage() {
        return garage;
    }

    public void setGarage(boolean garage) {
        this.garage = garage;
    }

    @Override
    public String toString() {
        return "House [floor=" + floor + ", garage=" + garage + ", garden=" + garden + "]";
    }

    
}
