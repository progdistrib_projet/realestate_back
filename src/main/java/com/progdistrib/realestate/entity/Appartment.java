package com.progdistrib.realestate.entity;

import javax.persistence.Entity;

@Entity
public class Appartment extends Property {

    private boolean balcony;
    private boolean parking;

    public Appartment() {
        super();
    }

    public Appartment(String name, String address, int rentPrice, boolean available, boolean balcony, boolean parking) {
        super(name, address, rentPrice, available);
        this.balcony = balcony;
        this.parking = parking;
    }

    public boolean isBalcony() {
        return balcony;
    }

    public void setBalcony(boolean balcony) {
        this.balcony = balcony;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    @Override
    public String toString() {
        return "Appartment [balcony=" + balcony + ", parking=" + parking + "]";
    }

    
}
