package com.progdistrib.realestate.utils;

public class Utils {

    public String verifyString(String newParam, String oldParam) {

        return newParam == null ? oldParam : newParam;
    }

    public int verifyLong(int newParam, int oldParam) {

        return newParam == 0 ? oldParam : newParam;
    }

}
