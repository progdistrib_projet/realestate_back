package com.progdistrib.realestate;

import com.progdistrib.realestate.dao.*;
import com.progdistrib.realestate.entity.*;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RealestateApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealestateApplication.class, args);
	}

	@Bean
	public CommandLineRunner appCommandLineRunner(UserRepository userRepository, PropertyRepository propertyRepository) {
		return (args) -> {

			User aurelie = new User("Aurelie", "password", "Aurelie", "D");
			userRepository.save(aurelie);

			User quentin = new User("Quentin", "password", "Quentin", "J");
			userRepository.save(quentin);

			Property hOne = new House("House", "6 rue de la paix", 1000, true, 2, true, true);
			hOne.setOwner(aurelie);
			propertyRepository.save(hOne);

			Property aOne = new Appartment("Appart", "55 avenue Leclerc", 700, true, true, true);
			aOne.setOwner(quentin);
			propertyRepository.save(aOne);
		};

	}
}
