FROM openjdk:13-alpine

LABEL maintainer="q.jeannot@outlook.fr"

COPY ./realestate_back/build/libs/realestate-0.0.1-SNAPSHOT.jar realestate.jar

EXPOSE 8080

CMD ["java","-jar","realestate.jar"]
