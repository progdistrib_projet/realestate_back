# Application Web de gestion de biens immobiliers

---

## Sommaire

- [Résumé](#résumé)
- [Installation](#installation)
- [User Stories](#users_stories)
- [Liste des endpoints](#endpoints)
- [Modèles de données](#modèles)

---

## Résumé

Cette application a pour objectif de permettre à ses utilisateurs de pouvoir gérer ses biens immobiliers. En effet, ils pourront ajouter des maisons ou appartements, afin que d'autres puissent les louer.

---

## Installation

### Projet Spring Boot existant
*Récupération du repository :
  `git clone https://gitlab.com/progdistrib_projet/realestate_back.git`
  
### Création d'un projet Spring Boot
* Générer un projet Spring Gradle avec [Spring Initializer](`https://start.spring.io/`). 
  * Utiliser Gradle pour la gestion des dépendances
  * Choisir le langage Java
  * Prendre la dernière version stable de Spring Boot (2.4.0)
  * Choisir le packaging Jar
  * Choisir les librairies nécessaires : 
    * Spring Data JPA
    * DevTools
    * Spring Web
    * Spring Security
    * D'autres librairies pourront être éventuellement ajoutées ultérieurement. 


---

## Les Users Stories

### Utilisateur non-authentifié : Client

Je souhaite m'authentifier

---

### Utilisateur authentifié : Bailleur

Je souhaite afficher tout les biens disponibles

Je souhaite louer un bien

Je souhaite rendre le bien loué

Je souhaite mettre en location un bien

Je souhaite afficher mes biens en location

Je souhaite modifier mon bien

Je souhaite supprimer mon bien 

---

## Endpoints

### Utilisateur non connecté

|   TYPE|    NOM  |  PARAMETRES |   DESCRIPTION|
|---|---|---|---|
| POST  |  /LOGIN | username, password    |   Authentification|
| POST  |  /REGISTER | username, password, firstname, lastname    | Inscription|



### Utilisateur connecté

|   TYPE|    NOM  |  PARAMETRES |   DESCRIPTION|
|---|---|---|---|
| GET |/PROPERTIES |   |   Récupération des biens d’un utilisateur|
| GET |/PROPERTIES/AVAILABLE/<:IDUSER> |   |   Récupération des biens disponibles à la location pour un utilisateur|
| GET |/PROPERTIES/USER/<:IDUSER> |   |   Récupération des biens appartenant à un utilisateur|

| POST |/HOUSE | Name, address, rentPrice, floor, garage, garden |  Ajouter un bien |
| POST |/APPARTMENT | Name, address, rentPrice, type, parking, balcony |  Ajouter un bien |

| PUT | /PROPERTY/<:PROPERTYID>| Les attibuts du bien à modifier |  Modifier un bien qui est déjà inscrit|
| DELETE |/PROPERTY/<:PROPERTYID> |  |   Supprimer un bien|
| GET |/PROPERTY/RENT/<:IDUSER> |   |   Récupération du bien loué par l'utilisateur|



## Modèles

### User

| Nom  | Type  |
|---|---|
| username, password|  gérés par Spring Security|
| firstName | String, required
 lastName| String, required

### Property

| Nom  | Type  |
|---|---|
| name | String, required  |
| address  | String, required  |
| rent  | Number, required  |
| owner  | Reference User, required  |
| available  | Boolean, required  |

### House subclass

| Nom  | Type |
|---|---|
| floor | Integer, required
 garden| Boolean
 garage | Boolean

### Appartment subclass

| Nom  | Type |
|---|---|
 balcony| Boolean
 parking | Boolean
